;; Navigation

(require 'ido)
(setq ido-everywhere t)
(setq ido-separator "\n")
(ido-mode t)
(setq ido-ignore-buffers '("^ " "*Completions*" "*Shell Command Output*"
                           "*Messages*" "Async Shell Command"
                           "*compilation*"))

(setq inhibit-eol-conversion t)

(windmove-default-keybindings 'meta)
(setq line-number-mode t)
(setq column-number-mode t)
(menu-bar-mode -1)
(setq linum-format "%3d  ")

(global-linum-mode 1)
(global-hl-line-mode 1)
(add-hook 'term-mode-hook (lambda () (linum-mode -1)))
(add-hook 'gdb-mode-hook (lambda () (linum-mode -1)))
(add-hook 'compilation-mode-hook (lambda () (linum-mode -1)))

(set-face-background 'hl-line "#313131")
(set-face-foreground 'highlight nil)

(show-paren-mode 1)

(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)
(setq ibuffer-show-empty-filter-groups nil)
(setq ibuffer-saved-filter-groups
      (quote (("default"
               ("C/C++" (or
                         (mode . c-mode)
                         (mode . c++-mode)))
               ("Python" (mode . python-mode))
               ("Documents" (or
                             (mode . markdown)
                             (mode . latex)))
               ("Emacs" (or
                         (name . "^.emacs$")
                         (mode . "Custom")))
               ("dired" (mode . dired-mode))))))

(add-hook 'ibuffer-mode-hook
	  (lambda ()
	    (ibuffer-switch-to-saved-filter-groups "default")))

;;;; UI
(if (display-graphic-p)
    (progn (tool-bar-mode -1)
	   (scroll-bar-mode -1)))

;; Coding
(global-set-key (kbd "C-c x") 'compile)
(global-set-key (kbd "C-c c") 'recompile)

(global-set-key (kbd "C-x g") 'magit-status)

;; Package Manager

(require 'package)
(add-to-list 'package-archives '("melpa"     . "http://melpa.milkbox.net/packages/"))
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Coding Style

(setq c-default-style "bsd"
      c-basic-offset 2)

(setq-default indent-tabs-mode nil)
(setq tab-width 2)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'text-mode-hook 'turn-on-auto-fill)

(setq custom-file "~/.emacs-custom.el")
(load custom-file 'noerror)

(if (require 'smart-mode-line nil 'noerror)
    (progn (setq sml/theme 'dark)
	   (sml/setup)))

(if (require 'rainbow-delimiters nil 'noerror)
    (progn (setq rainbow-delimiters-max-face-count 6)
	   (add-hook 'lisp-mode-hook       'rainbow-delimiters-mode)
	   (add-hook 'emacs-lisp-mode-hook 'rainbow-delimiters-mode)
	   (set-face-foreground 'rainbow-delimiters-depth-1-face "color-62")
	   (set-face-foreground 'rainbow-delimiters-depth-2-face "color-27")
	   (set-face-foreground 'rainbow-delimiters-depth-3-face "color-34")
	   (set-face-foreground 'rainbow-delimiters-depth-4-face "color-190")
	   (set-face-foreground 'rainbow-delimiters-depth-5-face "color-202")
	   (set-face-foreground 'rainbow-delimiters-depth-6-face "color-160")
	   (set-face-foreground 'rainbow-delimiters-unmatched-face "brightred")))

(if (require 'org-trello nil 'noerror)
    (add-hook 'org-mode-hook 'org-trello-mode))

(if (require 'emms-setup nil 'noerror)
    (progn (emms-standard)
           (emms-default-players)))

(if (require 'multi-term nil 'noerror)
    (progn (setq multi-term-program "/bin/bash")))
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(if (require 'auctex-latexmk)
    (auctex-latexmk-setup))

(dolist (hook '(text-mode-hook latex-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))

(dolist (hook '(change-log-mode-hook log-edit-mode-hook))
  (add-hook hook (lambda () (flyspell-mode -1))))
