export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
export EDITOR=emacs

alias clean="rm *~ *# .*~ .*# 2>/dev/null;"
alias sudo="sudo "
alias s="cd .."