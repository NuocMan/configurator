local ret_status="%(?:%{$fg_bold[green]%}:%{$fg_bold[red]%}[%?] %s)"

PROMPT=$(tput cup $LINES)'${ret_status}%{$fg_bold[black]%}(%{$fg_bold[blue]%}%n%{$fg_bold[black]%}@%{$fg_bold[blue]%}%M%{$fg_bold[black]%})(%{$fg_bold[blue]%}%j%{$fg_bold[black]%})(%{$fg_bold[green]%}%~%{$fg_bold[black]%})%{$fg_bold[blue]%}
$(git_prompt_info)%{$fg_bold[blue]%}$ %{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%})%{$fg[red]%}X %{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})%{$fg[yellow]%}V %{$reset_color%}"
