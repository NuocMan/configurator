export EDITOR='emacs'
export BROWSER='firefox'

alias s='cd ..'
alias clean="unsetopt NOMATCH; rm -vf *~ *#; setopt NOMATCH;"
alias xup='xrdb -merge ~/.Xresources'
alias sudo='sudo '

setopt histignorespace
