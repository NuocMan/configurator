#!/bin/bash

source conf_info

USAGE="\
usage: $0 [--help]

This tool is used to save and recover configuration files\
"

declare -A opts

function getargs {
    local name=$1; shift;
    local nb=0

    while [[ $1 != -* && $# != 0 ]]; do
        args+="$1 "; shift;
        (( ++nb ))
    done

    opts[$name]=$args
    return $nb
}

function help {
    echo "$USAGE"
}

function list {
    local i=0 conf
    local -A args

    while (( $# )); do
        if [[ -n ${confs[$1]} ]]; then
            args[$1]="X"
        else
            echo "ERROR:$FUNCNAME: Invalid argument $1" >&2
        fi
        shift
    done

    for conf in "${!confs[@]}"; do
        if [[ -z "${args[@]}" || -n "${args[$conf]}" ]]; then
            echo "$conf"
            echo -e "\t${confs[$conf]}"
        fi
    done
}

function save_conf {
    local dir_path="$directory/$1"
    local conf_path conf

    for conf in ${confs[$1]}; do
        conf_path="$dir_path/$conf"
        conf="${conf/#\~/$HOME}"
        [[ -a $conf ]] || {
            echo "ERROR:$FUNCNAME: $conf does not exist" >&2
            continue
        }
        mkdir -p "${conf_path%/*}"
        cp -v $conf $conf_path
    done
}

function save {
    while (( $# )); do
        if [[ -n "${confs[$1]}" ]]; then
            save_conf $1
        else
            echo "ERROR:$FUNCNAME: Invalid argument $1" >&2
        fi
        shift
    done
}

function restore_conf {
    local dir_path="$directory/$1"
    local conf_path conf

    for conf in ${confs[$1]}; do
        conf_path="$dir_path/$conf"
        conf="${conf/#\~/$HOME}"
        [[ -a $conf_path ]] || {
            echo "ERROR:$FUNCNAME: $conf_path does not exist" >&2
            continue
        }
        mkdir -p "${conf%/*}"
        cp -v $conf_path $conf
    done
}

function restore {
    while (( $# )); do
        if [[ -n "${confs[$1]}" ]]; then
            restore_conf $1
        else
            echo "ERROR:$FUNCNAME: Invalid argument $1" >&2
        fi
        shift
    done
}


if (( $# == 0 )); then
    help
    exit 1
fi

while (( $# )); do
    case $1 in
        --help)
            help
            exit 0
            ;;
        --list | --save | --restore)
            getargs "$@"
            shift $?
            ;;
        *)
            help
            exit 1
            ;;
    esac
    shift;
done

[[ ${!opts[@]} == *"--list"* ]] && list ${opts[--list]}
[[ ${!opts[@]} == *"--save"* ]] && save ${opts[--save]}
[[ ${!opts[@]} == *"--restore"* ]] && restore ${opts[--restore]}
